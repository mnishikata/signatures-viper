//
//  ListWireframe.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 30/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation
import MessageUI

class ListWireframe: NSObject, MFMailComposeViewControllerDelegate {
  weak var listPresenter: ListPresenter?
  
  convenience init(presenter: ListPresenter) {
    self.init()
    self.listPresenter = presenter
  }
  
  func showAddInterface(for viewController: UIViewController) {
    if let nav = viewController.storyboard?.instantiateViewController(withIdentifier: "GLPadNavigationController") as! UINavigationController? {
      if let _ = nav.viewControllers.first as? GLPadViewController {
        viewController.present(nav, animated: true, completion:nil)
      }
    }
  }
  
  func showMailComposeViewController(for viewController: UIViewController) {
    guard MFMailComposeViewController.canSendMail() == true else { return }
    guard let attachmentUrl = listPresenter?.pdf() else { return }
    let controller = MFMailComposeViewController()
    controller.modalPresentationStyle = .formSheet
    guard let data = try? Data(contentsOf: attachmentUrl) else { return }
    controller.addAttachmentData(data, mimeType: "application/pdf", fileName: "Document.pdf")
    controller.setToRecipients(UserDefaultsStorage().defaultEmail?.components(separatedBy: ","))
    controller.mailComposeDelegate = self
    viewController.present(controller, animated: true, completion: nil)
  }
  
  func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
    
    controller.dismiss(animated: true, completion: nil)
  }
}
