//
//  RealmRepository.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation
import RealmSwift

final class RealmRepository {
  
  static var shared = RealmRepository()
  
  lazy private var realm: Realm = {
    let realm = try! Realm()
    return realm
    }()
  
  var records: Results<Record> {
    let objects = realm.objects(Record.self).filter("isDeleted != %@", true).sorted(byProperty: "creationDate", ascending: !UserDefaultsStorage().descendingSortOrder)
    
    return objects
  }
  
  func add(_ record: Record) {
    try! realm.write {
      let date = NSDate()
      record.timestamp = date
      record.creationDate = date

      record.uuid = shortUUIDString()
      realm.add(record)
    }
  }
  
  func update(_ record: Record, checked: Bool) {
    try! realm.write {
      record.checked = checked
      record.timestamp = NSDate()
      realm.add(record, update: true)
    }
  }
  
  func delete(_ record: Record) {
    try! realm.write {
      record.isDeleted = true
      record.timestamp = NSDate()
      record.printName = nil
      record.signature = nil
      realm.add(record, update: true)
    }
  }
  
  func deleteAll() {
    try! realm.write {
      
      for record in records {
        autoreleasepool {
          record.isDeleted = true
          record.timestamp = NSDate()
          record.printName = nil
          record.signature = nil
          realm.add(record, update: true)
        }
      }
    }
  }
  
  
  func writeRecordRealmCopy(toFile newURL: URL) throws {
    objc_sync_enter(self)
    
    if FileManager.default.fileExists(atPath: newURL.path) {
      try FileManager.default.removeItem(at: newURL)
    }
    
    try realm.writeCopy(toFile: newURL)
    objc_sync_exit(self)
  }
  
  func mingleRecords(from url: URL, completion: (()->Void)?) {
    
    let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
				queue.async {
          
          objc_sync_enter(self)
          print("== LOAD FROM ICLOUD \(url) ==")
          
          let config = Realm.Configuration(
            
            // Get the URL to the bundled file
            fileURL: url,
            // Open the file in read-only mode as application bundles are not writeable
            /*
             encryptionKey: "fiasautodsycbathigjagyekeowhewifiasautodsycbathigjagyekeowhewiab".data(using: .utf8, allowLossyConversion: false),*/
            readOnly: true)
          
          // Open the Realm with the configuration
          guard let iCloudRealm = try? Realm(configuration: config) else { return }
          
          
          let objs =  iCloudRealm.objects(Record.self).sorted(byProperty: "timestamp", ascending: false)
          try? self.realm.write {
            for obj in objs {
              autoreleasepool {
                let localCounterpart = self.realm.objects(Record.self).filter("uuid == %@", obj.uuid)
                if let localRecord = localCounterpart.first {
                  
                  if localRecord.timestamp!.timeIntervalSinceReferenceDate < obj.timestamp!.timeIntervalSinceReferenceDate {
                    self.realm.create(Record.self, value: obj, update: true)
                    
                  }else {
                    
                    // END ?
                    
                    //break
                  }
                  
                }else {
                  self.realm.create(Record.self, value: obj)
                  
                }
              }
            }
          }
          objc_sync_exit(self)
          
          if completion != nil {
            DispatchQueue.main.async(execute: completion!)
          }
    }
  }
}
