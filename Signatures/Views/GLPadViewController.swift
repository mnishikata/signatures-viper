//
//  GLPadViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

class GLPadBaseClass: UIViewController, WritepadInputViewDelegate {
  lazy var glPadPresenter = GLPadPresenter()

  @IBOutlet weak var glView: WritepadPolygonGLView!

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    glView.lineWidth = 10.0
    glView.lineHeight = 120
    glView.strokeType = MNStrokeTypeFude
    glView.isDoubleRow = false
    glView.drawsBaselines = false
    glView.delegate = self
    
    navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }

  func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, touchBegan touch: TouchHistory!, event: UIEvent!) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, touchEnded touch: TouchHistory!, event: UIEvent!, isDoubleTap doubleTap: Bool) {
  }
  func fudeLineView(_ view: WritepadPolygonGLView!, willDrawLines polygonDraw: PolygonDraw!, points: UnsafeMutablePointer<LinePoint>!, count: UInt) {
  }
  
  func pendingTanzaku(_ tanzaku: MNTanzaku) {
  }
  
  func backgroundColor() -> MyColor {
    let color = MyColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
    return color
  }
  
  func convertDot() -> Bool {
    return true
  }
  
  func convertDotsType() -> Int {
    return 0
  }
  
  func enter(_ tanzaku:MNTanzaku) {
  }
}


class GLPadViewController: GLPadBaseClass {
  
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var clearButton: UIButton!

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let tanzaku = glPadPresenter.drawData.printTanzaku
    glView.clear(self)
    glView.push(tanzaku)
  }
  
  @IBAction func clear(_ sender: Any) {
    glView.clear(self)
    nextButton.isEnabled = false
    clearButton.isEnabled = false
  }
  
  @IBAction func cancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    glView.enter(self)
    if let controller = segue.destination as? GLPadViewController2 {
      controller.glPadPresenter.drawData = glPadPresenter.drawData
    }
  }
  
  override func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
    nextButton.isEnabled = true
    clearButton.isEnabled = true
  }
 
  override func enter(_ tanzaku:MNTanzaku) {
    glPadPresenter.drawData.printTanzaku = tanzaku
  }
}

class GLPadViewController2: GLPadBaseClass {
  
  
  @IBOutlet weak var doneButton: UIButton!
  @IBOutlet weak var clearButton: UIButton!

  @IBAction func clear(_ sender: Any) {
    glView.clear(self)
    doneButton.isEnabled = false
    clearButton.isEnabled = false
  }
  
  @IBAction func cancel(_ sender: Any) {
    dismiss(animated: true, completion: nil)
  }
  
  @IBAction func done(_ sender: Any) {
    glView.enter(self)
    
    if glPadPresenter.addRecord() {
    dismiss(animated: true, completion: nil)
    }else {
      ShowAlertMessage("Error", "Could not save", self)
    }
  }

  override func fudeLineView(_ view: WritepadPolygonGLView!, strokeAdded stroke: MNStroke16!) {
    doneButton.isEnabled = true
    clearButton.isEnabled = true
  }
  
  override func enter(_ tanzaku:MNTanzaku) {
    glPadPresenter.drawData.signatureTanzaku = tanzaku
  }
  
}
