//
//  ListTableViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit
import RealmSwift
import Messages
import MessageUI

class ListTableViewController: UITableViewController, ListInterface {
  
  lazy var listPresenter: ListPresenter = { [unowned self] in
    var listPresenter = ListPresenter(interface: self)
    return listPresenter
  }()
  
  func reload() {
    tableView.reloadData()
  }
  
  @IBAction func export(_ sender: Any) {
    guard let nav = self.navigationController else { return }
    listPresenter.showMailComposeViewController(for: nav)
  }
  
  @IBAction func edit(_ sender: Any) {
    self.setEditing(!self.isEditing, animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    self.navigationController?.isToolbarHidden = false
    tableView.reloadData()

    NotificationCenter.default.addObserver(self, selector: #selector(reload), name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillAppear(animated)
    NotificationCenter.default.removeObserver(self, name: DataSourceNotification.setNeedsDisplay, object: nil)
  }
  
  @IBAction func add() {
    guard let nav = self.navigationController else { return }
    listPresenter.showAddInterface(for: nav)
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return listPresenter.numberOfRecords
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return listPresenter.rowHeight
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecordCell
    
    guard listPresenter.numberOfRecords > indexPath.row else { return cell }
    return listPresenter.configure(cell: cell, atIndex: indexPath.row)
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    guard listPresenter.numberOfRecords > indexPath.row else { return }

    listPresenter.deleteRecord(at: indexPath.row)
    tableView.deleteRows(at: [indexPath], with: .automatic)
  }
  
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
}

