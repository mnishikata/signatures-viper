//
//  SettingsViewController.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 24/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation


class SettingsViewController: UIViewController {
  
  lazy var settingsPresenter = SettingsPresenter()

  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var sortSwitch: UISwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.preferredContentSize = CGSize(width:320, height: 300)
  }
  
  @IBAction func sortChanged(_ sender: Any) {
    settingsPresenter.descendingSortOrder = !sortSwitch.isOn
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    sortSwitch.isOn = !settingsPresenter.descendingSortOrder
    emailField.text = settingsPresenter.defaultEmail
  }
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    settingsPresenter.defaultEmail = emailField.text
  }
}
