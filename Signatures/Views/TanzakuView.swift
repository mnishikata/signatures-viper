//
//  TanzakuView.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 23/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import UIKit

class TanzakuView: UIView {

  let pointSize: CGFloat = 24.0
  let basePointSize: CGFloat = 12.0
  let writingPadToGlyphScale: CGFloat = 0.1
  var tanzaku: MNTanzaku! {
    didSet {
      setNeedsDisplay()
    }
  }
  var ascent: CGFloat {
    return tanzaku.occupiedRect.maxY * writingPadToGlyphScale * ( pointSize / basePointSize );
  }
  var width: CGFloat {
    return tanzaku.occupiedRect.size.width * writingPadToGlyphScale * ( pointSize / basePointSize);

  }
  var descent: CGFloat {
    return max(0, -tanzaku.occupiedRect.origin.y) * writingPadToGlyphScale * ( pointSize / basePointSize);

  }
  
  override func draw(_ rect: CGRect) {
    guard let ctx = UIGraphicsGetCurrentContext() else { return }
    guard tanzaku != nil else { return }
    tanzaku.draw(in: rect, in: ctx)
  }
  
}


final class CheckButton: UIButton {
  var action:(()->Void)? = nil
}

final class RecordCell: UITableViewCell {
  
  @IBOutlet weak var imageView1: TanzakuView!
  @IBOutlet weak var imageView2: TanzakuView!
  @IBAction func checkAction(_ sender: CheckButton) {
    sender.action?()
  }
  @IBOutlet weak var checkButton: CheckButton!
  @IBOutlet weak var timestampLabel: UILabel!
}


extension MNTanzaku {
  
  func draw(in rect: CGRect, in ctx: CGContext) {
    ctx.saveGState()
    ctx.setLineCap(.round)
    ctx.setLineJoin(.round)
    
    let scale =  min( rect.size.width / occupiedRect.size.width, rect.size.height / occupiedRect.size.height )
    var transform = CGAffineTransform.identity
    
    transform = CGAffineTransform( translationX: rect.origin.x , y: rect.origin.y + rect.size.height + self.occupiedRect.origin.y * scale )
    ctx.concatenate(transform)
    
    transform = CGAffineTransform( scaleX: scale, y: -scale )
    ctx.concatenate(transform)
    
    for stroke in self.strokes {
      (stroke as! MNStroke16).draw(in: ctx )
    }
    
    ctx.restoreGState()
  }
  
  convenience init?(from data: Data) {
    if let dict1 = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [AnyHashable: Any] {
      self.init(dictionary: dict1)
    }else {
      return nil
    }
  }
}
