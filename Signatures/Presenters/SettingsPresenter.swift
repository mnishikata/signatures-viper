//
//  SettingsPresenter.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 30/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation

class SettingsPresenter {
  var descendingSortOrder: Bool {
    get {
      return UserDefaultsStorage().descendingSortOrder
    }
    set {
      UserDefaultsStorage().descendingSortOrder = newValue
      NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)
      
    }
  }
  
  var defaultEmail: String? {
    get {
      return UserDefaultsStorage().defaultEmail
    }
    set {
      UserDefaultsStorage().defaultEmail = newValue
    }
  }
}
