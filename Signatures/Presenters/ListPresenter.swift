//
//  ListPresenter
//  Signatures
//
//  Created by Masatoshi Nishikata on 29/01/17.
//  Copyright (c) 2017 Catalystwo. All rights reserved.
//

import UIKit
import RealmSwift

// NOTIFICATION

enum DataSourceNotification {
  static let selectionDidChange = Notification.Name("DataSourceSelectionDidChange")
  static let setNeedsDisplay = Notification.Name("DataSourceSetNeedsDisplay")
}

protocol ListInterface {
  func reload()
}

final class ListPresenter: NSObject
{
  var interface: ListInterface!
  lazy var wireframe: ListWireframe = { [unowned self] in
    let wireframe = ListWireframe(presenter: self)
    return wireframe
  }()
  //MARK:-
  var errorHandler: ((_ error:Error) -> Void)?

  convenience init(interface: ListInterface) {
    self.init()
    self.interface = interface
    let iCloudRepo = ICloudRepository.shared
    iCloudRepo.cloudDidUpdateHandler = { url in self.cloudDidChange(at: url) }
    iCloudRepo.conflictHandler = { ( allVersions: [NSFileVersion], decisionHandler: @escaping ICloudConflictDecisionHandler) in
      print("### Handling Conflicts ###")
      decisionHandler(allVersions.first!)
    }
    iCloudRepo.errorHandler = errorHandler // TODO
    iCloudRepo.startICloudQuery()
  }

  //MARK:- ROUTING

  func showAddInterface(for viewController: UIViewController) {
    wireframe.showAddInterface(for: viewController)
  }
  
  func showMailComposeViewController(for viewController: UIViewController) {
    wireframe.showMailComposeViewController(for: viewController)
  }
  
  //MARK:- CONFIGURE UI
  
  func configure(cell: RecordCell, atIndex index: Int) -> RecordCell {

    let record: Record = self.record(at: index)
    cell.timestampLabel.text = dateString(from: record.timestamp as Date)
    
    if let button = cell.checkButton {
      button.action = { [unowned button] in
        button.isSelected = !button.isSelected
        self.updateRecord(record, checked: button.isSelected)
      }
      
      button.tag = index
      button.setTitle(nil, for: .normal)
      button.setImage(checkImage(false), for: .normal)
      button.setImage(checkImage(true), for: .selected)
      button.isSelected = record.checked
    }
    
    if let tanzaku = MNTanzaku(from: record.printName) {
      cell.imageView1.tanzaku = tanzaku
    }
    
    if let tanzaku = MNTanzaku(from: record.signature) {
      cell.imageView2.tanzaku = tanzaku
    }
    
    return cell
  }
  
  let rowHeight: CGFloat = 75

  //MARK:- ICLOUD DOCUMENT
  
  func cloudDidChange(at url: URL) {
    
    RealmRepository.shared.mingleRecords(from: url) {
      self.interface?.reload()
    }
  }
  
  //MARK: - Record

  func updateRecord(_ record: Record, checked: Bool) {
    RealmRepository.shared.update(record, checked: checked)
    ICloudRepository.shared.saveToICloud()
  }
  
  func deleteRecord(at index: Int) {
    let record = self.record(at: index)
    RealmRepository.shared.delete(record)
    ICloudRepository.shared.saveToICloud()
  }
  
  func record(at index: Int) -> Record {
    let record = RealmRepository.shared.records[index]
    return record
  }
  
  var numberOfRecords: Int {
    return RealmRepository.shared.records.count
  }
  
  func deleteAll() {
    RealmRepository.shared.deleteAll()
    ICloudRepository.shared.saveToICloud()
    
    self.interface?.reload()
  }

  
  // UTILS
  
  func pdf() -> URL? {
    let records = RealmRepository.shared.records
    let height: CGFloat = 50
    let documentWidth: CGFloat = 768
    let tanzakuWidth: CGFloat = 200
    let size = CGSize(width: documentWidth, height: CGFloat(records.count) * height )
    let pathBoundingBox = CGRect(origin:CGPoint.zero, size:size)
    let folder = URL(fileURLWithPath: NSTemporaryDirectory())
    let destUrl = folder.appendingPathComponent("Document.pdf")
    
    if FileManager.default.fileExists(atPath: destUrl.path) {
      try? FileManager.default.removeItem(at: destUrl)
    }
    
    UIGraphicsBeginPDFContextToFile (
      destUrl.path,
      pathBoundingBox,
      nil
    )
    
    guard let ctx = UIGraphicsGetCurrentContext() else { return nil }
    ctx.setLineCap(.round)
    ctx.setLineJoin(.round)
    
    UIGraphicsBeginPDFPage()
    
    var y: CGFloat = 0
    for record in records {
      autoreleasepool {
        var x: CGFloat = 5
        let checked = record.checked
        guard let date = record.creationDate else { return }
        guard let print = MNTanzaku(from: record.printName) else { return }
        guard let signature = MNTanzaku(from: record.signature) else { return }
        
        var rect = CGRect(x: x, y: y, width: tanzakuWidth, height: height)
        print.draw(in: rect, in: ctx)
        x += tanzakuWidth
        
        rect = CGRect(x: x, y: y, width: tanzakuWidth, height: height)
        signature.draw(in: rect, in: ctx)
        
        x += tanzakuWidth
        
        let dateString = self.dateString(from: date as Date)
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.black]
        let attrString = NSAttributedString(string: dateString, attributes: attributes)
        
        attrString.draw(at: CGPoint(x: x, y: y + (height - attrString.size().height)/2))
        
        x += attrString.size().width
        
        let checkImage = self.checkImage(checked)
        checkImage.draw(in: CGRect(x: x + 5, y: y + 5, width: height - 10, height: height - 10))
        
        y += height
        
        let points = [CGPoint(x: 5, y: y - 0.5), CGPoint(x: documentWidth - 5, y: y - 0.5)]
        ctx.setLineWidth(0.5)
        ctx.setStrokeColor(UIColor.lightGray.cgColor)
        ctx.strokeLineSegments(between: points)
      }
    }
    
    UIGraphicsEndPDFContext()
    return destUrl
  }

  func checkImage(_ checked: Bool) -> UIImage {
    let checkImage = checked ? #imageLiteral(resourceName: "MNOutlineChecked") : #imageLiteral(resourceName: "MNOutlineUnchecked")
    return checkImage
  }
  
  lazy var dateFormatter :DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    return formatter
  }()
  
  func dateString(from date: Date) -> String {
    return dateFormatter.string(from: date)
  }
  
}
