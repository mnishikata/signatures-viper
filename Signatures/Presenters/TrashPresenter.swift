//
//  TrashPresenter.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 30/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation

class TrashPresenter {
  
  func deleteAll() {
    RealmRepository.shared.deleteAll()
    ICloudRepository.shared.saveToICloud()
    
    NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)
  }
}
