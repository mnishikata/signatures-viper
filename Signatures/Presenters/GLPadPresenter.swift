//
//  GLPadPresenter.swift
//  Signatures
//
//  Created by Masatoshi Nishikata on 30/01/17.
//  Copyright © 2017 Catalystwo. All rights reserved.
//

import Foundation

class GLPadPresenter {
  var drawData = DrawData()

  func addRecord() -> Bool {
    guard let print = drawData.printTanzaku else { return false }
    guard let signature = drawData.signatureTanzaku else { return false }
    
    let record = Record()
    guard let printData = try? PropertyListSerialization.data(fromPropertyList: print.dictionary(), format: .binary, options: 0) else { return false }
    record.printName = printData
    guard let signData = try? PropertyListSerialization.data(fromPropertyList: signature.dictionary(), format: .binary, options: 0) else { return false }
    record.signature = signData
 
    RealmRepository.shared.add(record)
    ICloudRepository.shared.saveToICloud()

    NotificationCenter.default.post(name: DataSourceNotification.setNeedsDisplay, object: nil, userInfo: nil)

    return true
  }
  

}
